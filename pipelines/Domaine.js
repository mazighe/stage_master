db.getCollection("scientificamerican.com.articles").aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$group: {
			_id: '$origin',
						NbArticles: {$sum: 1},
						Article: {$push: {createdDate: '$pub_date', nameAuthor: '$authors', title: '$title'}}
			}
		},

		// Stage 2
		{
			$sort: {
			NbArticles: -1
			}
		},

	]

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
