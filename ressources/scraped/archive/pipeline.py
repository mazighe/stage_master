# -*- coding: utf-8 -*-
import json
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import glob 
import os.path
import time 
import unique



start_time = time.time()

#Concatination des données
def merge_JsonFiles(filename):
	"""Concatination des différents fichiers json qui sont dans le dossier pointé par le chemin filename,
	 dans une base de données globale, qui sera transformer en DataFrame.
	
	Args:
		filename (path): Le chemin vers les différents fichiers json
	
	Returns:
		DataFrame: Un DataFrame qui est construit à partir du fichier json (résultat de la concatination)
					 qui contient tous les articles récupérer 
	"""
	result = list()
	for f in filename:
		with open(f, 'r', encoding='UTF-8') as infile:
			result.extend(json.load(infile))
			
	with open('../archive/resultat_pipelines/Base_de_donees_globale.json', 'w') as out:
		json.dump(result, out, indent=4)
	data = pd.DataFrame(result)
	return data

#Trouver le nombre de valeurs manquantes
def num_missing(df):
	"""Trouver le nombre de valeurs manquantes
	
	Args:
		df (DataFrame): Le DataFrame qui contient les informations sur les articles récuperé 
	
	Returns:
		int: Le nombre de champs vide
	"""
	return sum(df.isnull() | df.isin([""]))
#Supprimer les articles qui ont des champs vide

def supprimer_champs_vide(df):
	"""Supprimer les articles qui ont des champs vide
	
	Args:
		df (DataFrame): Le DataFrame qui contient les informations sur les articles récuperé
	
	Returns:
		DataFrame: Un DataFrame qui ne contient pas de champs vide 
	"""
	df = df.drop(df[df["origin"].isin([""])].index.tolist(), axis = 0)
	df = df.drop(df[df["keywords"].isin([""])].index.tolist(), axis = 0)
	df = df.drop(df[df["title"].isin([""])].index.tolist(), axis = 0)
	df = df.drop(df[df["authors"].isin([""])].index.tolist(), axis = 0)
	df = df.drop(df[df["pub_date"].isin([""])].index.tolist(), axis = 0)
	df = df.drop(df[df["link"].isin([""])].index.tolist(), axis = 0)
	df = df.drop(df[df["abstract"].isin([""])].index.tolist(), axis = 0)
	df = df.dropna(subset = ['pub_date', 'abstract', 'keywords', 'title', 'link', 'authors', 'origin'])
	return  df

#Trouver les articles double dans la base de données
def touver_double(df):
	"""Trouver les articles double dans la base de données				
	
	Args:
		df (DataFrame): Le DataFrame qui contient les informations sur les articles récuperé 
	
	Returns:
		Serie: Une serie des articles qui sont en double 
	"""
	return df[df.duplicated()]

#Supprimer les données double
def supprimer_double(df):
	"""Supprimer les données double
	
	Args:
		df (DataFrame): Le DataFrame qui contient les informations sur les articles récuperé 
	
	Returns:
		DataFrame: Un DataFrame sans les articles qui sont en double
	"""
	return df.drop_duplicates(subset = ['link', 'title', 'authors', 'keywords', 'pub_date', 'origin', 'image'], keep = 'first')

#Diagramme en bâtons pour tracer le nombre de champs complet dans la base de données
def complet(df):
	"""Diagramme en bâtons pour tracer le nombre de champs complet dans la base de données
	
	Args:
		df (DataFrame): Le DataFrame qui contient les informations sur les articles récuperé
	
	Returns:
		Diagramme: Diagramme en bâtons
	"""
	val1 = sum(pd.value_counts(df['link']))
	val2 = sum(pd.value_counts(df['origin']))
	val3 = sum(pd.value_counts(df['pub_date']))
	val4 = sum(pd.value_counts(df['keywords']))
	val5 = sum(pd.value_counts(df['abstract']))
	val6 = sum(pd.value_counts(df['title']))
	val7 = sum(pd.value_counts(df['authors']))
	val8 = sum(pd.value_counts(df['image']))
	val = [val1, val2, val3, val4, val5, val6, val7,val8]
	a = ['link', 'origin', 'pub_date', 'keywords', 'abstract', 'title', 'authors', 'image']
	return plt.bar(a, val, width = 0.2)


#Afficher le nomdre d'articles récupérer à partir de chaque site
def afficher_informations():
	print('-------------------------------------------------------------------')
	print('Le nombre total d_articles dans cette base de donnees est:', len(data), 'articles')
	print('-------------------------------------------------------------------')
	print('Le nombre d_articles double dans la base de donnees est:', len(data) - len(base_de_donnees_globale), 'articles')
	print('-------------------------------------------------------------------')
	print('Les articles qui sont en doubles:\n\n',double['link'].value_counts())
	print('-------------------------------------------------------------------')	
	print('Le nombre d_articles apres la supression des donnees double est:', len(base_de_donnees_globale), 'articles')	
	print('-------------------------------------------------------------------')
	print('Le nombre de champs vide dans toute la base de donnees est:', base_de_donnees_globale.isnull().sum().sum())
	print('-------------------------------------------------------------------')
	print("Les champs vide sur cahque colonne:\n",base_de_donnees_globale.apply(num_missing, axis=0))
	print('-------------------------------------------------------------------')
	print('Le nombre d_articles supprimer est:', len(data) - len(donnees_de_bonne_qualite), 'articles')
	print('-------------------------------------------------------------------')
	print('Le nombre d_articles  restant est:', len(donnees_de_bonne_qualite), 'articles' )
	print('-------------------------------------------------------------------')
	return

	

#Etape 1: Concatination des données
data = merge_JsonFiles(glob.glob("../archive/*.json")) 

#Etape 2: Trouver les aricles doubles et les supprimer 
double = touver_double(data)
base_de_donnees_globale = supprimer_double(data)

#Etape 3: Trouver les articles avec des champs vide et les supprimer 
donnees_de_bonne_qualite = supprimer_champs_vide(base_de_donnees_globale)
donnees_de_bonne_qualite.to_json (r'../archive/resultat_pipelines/Base_de_donnees_finale.json', orient ='records')



#Faire le traitement pour chaque site séparement
unique.articles()
unique.merge_JsonFiles(glob.glob("../archive/site_pip/*.json"))

#Afficher le temps d'execution
print('-------------------------------------------------------------------')
print("Temps d_execution : %s secondes ---" % (time.time() - start_time))

#Etape 4: Tracer les diagrammes et afficher les informations
complet(data)
plt.title("Nombre de valeurs complètes dans chaque chapms avant les traitement")
plt.show()
complet(donnees_de_bonne_qualite)
plt.title("Nombre de valeurs complètes dans chaque champs après le traitement")
plt.show()
afficher_informations()
