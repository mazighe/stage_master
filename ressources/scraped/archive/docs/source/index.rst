.. Pipeline documentation master file, created by
   sphinx-quickstart on Mon Jan 27 13:44:19 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

*************************************
Welcome to Pipeline's documentation!
*************************************


Introduction 
=============

Tandis que les volumes de données continuent d’augmenter à des rythmes effrénés, les entreprises utilisent les pipelines de données pour libérer la puissance de leurs données et répondre plus rapidement aux exigences.

Ce pipeline à pour objectif la transformation des données superflues ou invalides à des données de bonnes qualité. Il prend en entrée les données qui proviennent de différents sites  scientifique, et il nous renvoi un fichier JSON qui contient les données de bonne qualité.

**Etape 1:**
  * Regrouper les différentes bases de données dans une base de données globale.

**Etape 2:** 
  * Trouver tous les articles qui sont en double.
  * Supprimer ces articles qui sont en double.

**Etape 3:** 
  * Trouver les articles qui ont des champs vide (données non complète)dans la toute la base de données.
  * Supprimer tous les articles qui ont des champs vide.

**Etape 4:**   
  * Afficher quelques informations tels que le nombre d’articles on a en entrée et le nombre d’articles restant.
  * Tracer des diagramme pour faire une analyse statistique.  


.. image:: 
   pipeline.png
   :scale: 80 %
   :align: center


Mise en place
================
Pour le bon usage de ce pipeline il est préférable d'organiser votre répertoire de travail comme suit,
On place le code source du pipeline dans le dossier qui contient les différents articles récupérés, et dans ce répertoire en crée un sous dossier qu’on va appeler resultat_pipelines, ce dernier recevra le résultat de ce pipeline (une base de donnée globale et les données de bonne qualité). On peut  voir  ci-dessous  l’arborescence qui illustre l’organisation de notre répertoire de travail, sachant que les  fichiers JSON contiennent les informations des articles récupérés de chaque site, et « pipeline.py » c’est le code source du pipeline.

.. image:: 
   tree.png
   :scale: 80 %
   :align: center


Pour exécuter ce pipeline il suffit juste de lancer un terminal dans le dossier qui contient le fichier   «pipeline.py», et taper la commande suivante:

.. code-block:: python  
   
   py pipeline.py    

Code source
==========
Dans cette partie je vais vous présenter en détails les principales  fonctions qui interviennent dans les différentes étapes de ce pipeline. 

.. py:function:: def merge_JsonFiles(Args)

   :description: Concatination des différents fichiers json qui sont dans le dossier pointé par le chemin filename en une base de données globale, qui sera transformer en DataFrame.	
   :param  arg: Le chemin vers les différents fichiers json
   :type arg: path
   :return: Un DataFrame qui est construit à partir du fichier json (résultat de la concatination) qui contient tous les articles récupérer
   :rtype: DataFrame

.. py:function:: def num_missing(Args)

   	:description: Trouver le nombre de valeurs manquantes	
	:param arg: Le DataFrame qui contient les informations sur les articles récuperés
	:type arg: DataFrame	
	:return: Le nombre de champs vides
	:rtype: int

.. py:function:: def supprimer_champs_vide(Args)

   	:description: Supprimer les articles qui ont des champs vide	
	:param arg: Le DataFrame qui contient les informations sur les articles récuperés
	:type arg: DataFrame	
	:return: Un DataFrame qui ne contient pas de champs vide
	:rtype: DataFrame

.. py:function:: def supprimer_double(Args)

   	:description: Supprimer les articles qui sont en double	
	:param arg: Le DataFrame qui contient les informations sur les articles récuperés
	:type arg: DataFrame	
	:return: Un DataFrame qui ne contient pas d'articles double
	:rtype: DataFrame

.. py:function:: def complet(Args)

   	:description: Tracer des diagramme en bâtons pour illuster le nombre de champs complet dans la base de données	
	:param arg: Le DataFrame qui contient les informations sur les articles récuperés
	:type arg: DataFrame	
	:return: Diagramme en bâtons
	:rtype: Diagramme

.. py:function:: def touver_double(Args)

   	:description: Trouver les articles double dans la base de données
	:param arg: Tracer des diagramme en bâtons pour illuster le nombre de champs complet dans la base de données
	:type arg: DataFrame	
	:return: Une serie d'articles qui sont en double
	:rtype: objet

Exemples de test
=========
Dans cette partie je vais vous donner un exemple d’illustration sur l’intérêt de l’utilisation de ce pipeline pour le traitement de données évolutives qui proviennent de différentes sources, pour avoir des données de bonne qualité prête à être exploitées.

**Les entrées:**

  * 9  fichiers json qui contient tous les articles récupérer à partir de chaque site. 
  * Un total de 54839 articles.

**Les sorties:** 

  * Un fichier json qui contient tous les articles.
  * Un fichier json qui contient les articles de bonne qualité.
  * Le nombre d’articles trouver en double est 1250 articles.
  * Le nombre de champs vide trouver est 78546. 
  * Le nombre d’articles restant est 11664.
  * Le temps d'exécution est de  12.36 secondes. 

.. image:: 
   avant.png
   :scale: 70 %
   :align: center

.. image:: 
   apres.png
   :scale: 80 %
   :align: center
**Discussion des résultats:**

* On remarque de 54839 articles qu’on a au début il restait que 11664 articles (21.26%) ce qui implique qu’aujourd’hui on trouvent des données erronés partout, et souvent incomplètes, obsolètes ou incorrectes d’où l’intérêt de faire appel à un pipeline de traitement de données.

Conclusion
=========
En matière de traitement et d’intégration des données, le temps est un luxe que les entreprises ne peuvent plus s'offrir. Le but de tout pipeline de données est d’intégrer des données volumineuses  pour livrer des données exploitables, en temps quasi réel.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   


