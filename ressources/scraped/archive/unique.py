# -*- coding: utf-8 -*-
import json
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import glob 
import os.path 


def supprimer_double_site(df):
	"""Supprimer les données double
	
	Args:
		df (DataFrame): Le DataFrame qui contient les informations sur les articles de la base de donées 
	
	Returns:
		DataFrame: Un DataFrame sans les articles qui sont en double
	"""
	return df.drop_duplicates( subset = df.columns, keep = 'first')


def supprimer_vide_site(df):
	"""Supprimer les données incomplètes
	
	Args:
		df (DataFrame): Le DataFrame qui contient les informations sur les articles de la base de donées 
	
	Returns:
		DataFrame: Un DataFrame sans les articles qui ont des données incomplètes
	"""
	for x in df:
		df = df.drop(df[df[x].isin([""])].index.tolist(), axis = 0)
	df = df.dropna(subset = df.columns)
	return df

def traitement(filename):
	df = pd.read_json(filename, encoding='UTF-8')
	print(filename,' ----> ', len(df), ' articles')
	chemin = os.path.join("../archive/site_pip", filename)
	df = supprimer_double_site(df)
	df = supprimer_vide_site(df)	
	df.to_json (chemin, orient ='records')
	return

def articles():
	traitement('academic.oup.com.articles.json')
	traitement('iopscience.iop.org.articles.json')
	traitement('jeb.biologists.org.2019.articles.json')
	traitement('sciencemag.org.articles.json')
	traitement('scientificamerican.com.articles.json')
	return

def merge_JsonFiles(filename):
	"""Concatination des différents fichiers json qui sont dans le dossier pointé par le chemin filename,
	 dans une base de données globale, qui sera transformer en DataFrame.
	
	Args:
		filename (path): Le chemin vers les différents fichiers json
	
	Returns:
		DataFrame: Un DataFrame qui est construit à partir du fichier json (résultat de la concatination)
					 qui contient tous les articles récupérer 
	"""
	result = list()
	for f in filename:
		with open(f, 'r', encoding='UTF-8') as infile:
			result.extend(json.load(infile))
			
	with open('../archive/resultat_pipelines/Donnees.json', 'w') as out:
		json.dump(result, out, indent=4)
	data = pd.DataFrame(result)
	return data




